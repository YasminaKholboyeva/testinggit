import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose the number of month to know which season it is");
        Integer inputNumber = scanner.nextInt();
        switch (inputNumber)
        {
            case 1:
                System.out.println("You choose January which is in the winter season.");
                break;
            case 2:
                System.out.println("You choose February which is in the winter season.");
                break;
            case 3:
                System.out.println("You choose March which is in the spring season.");
                break;
            case 4:
                System.out.println("You choose April which is in the spring season.");
                break;
            case 5:
                System.out.println("You choose May which is in the spring season");
                break;
            case 6:
                System.out.println("You choose June which is in the summer season.");
                break;
            case 7:
                System.out.println("You choose July which is in the summer season");
                break;
            case 8:
                System.out.println("You choose August which is in the summer season.");
                break;
            case 9:
                System.out.println("You choose September which is in the autumn season.");
                break;
            case 10:
                System.out.println("You choose October which is in the autumn season.");
                break;
            case 11:
                System.out.println("You choose November which is in the autumn season.");
                break;
            case 12:
                System.out.println("You choose December which is in the winter season.");
                break;
            default:
                System.out.println("ERROR");
        }
    }
}